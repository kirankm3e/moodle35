<?php
 require_once('../../config.php');
 $PAGE->set_context(get_context_instance(CONTEXT_SYSTEM));
 $PAGE->set_url('/blocks/simplehtml/requirejs_init.php');
 $PAGE->requires->js('/blocks/simplehtml/requirejs_init.js');
 $PAGE->requires->string_for_js('course', 'moodle');
 $PAGE->requires->js_init_call('lang');
 $PAGE->requires->js_init_call('hello2', array('Hello', $USER->username));
 echo $OUTPUT->header();
 ?>
 <script>
 var closebutton = Y.Node.create('<input type="button" class="btn btn-secondary" />');
 echo closebutton;
 </script>
 <?php
 echo get_user_field_name($field);
 echo '<p onclick="myfun()" >Hello</p>
 <script>function myfun1(){
    alert("welcome");
}</script>';
 //echo $CFG->wwwroot;
 echo $OUTPUT->footer();
?>