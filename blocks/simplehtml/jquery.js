 // Get the modal
 var modal = document.getElementById('myModal');

 // Get the button that opens the modal
 var btn = document.getElementById("myBtn");
 
 // Get the <span> element that closes the modal
 var span = document.getElementsByClassName("close")[0];
 
 // When the user clicks the button, open the modal 
 btn.onclick = function() {
   modal.style.display = "block";
   grade();
 }
 
 // When the user clicks on <span> (x), close the modal
 span.onclick = function() {
   modal.style.display = "none";
 }
 
 // When the user clicks anywhere outside of the modal, close it
 window.onclick = function(event) {
   if (event.target == modal) {
     modal.style.display = "none";
   }
 }

function myfunction(num1, num2) {
    alert(num1 + " + " + num2 + " = " + (num1 + num2) );
    var value = document.getElementById("txt1").value;
    document.getElementById("demo").innerHTML = value;
}

function grade() {
    //document.getElementById('demo').innerHTML ="";
    var score = document.getElementById('per').value;
    if (score >= 0 && score < 35) {
      document.getElementById('msg').innerHTML = "You Have Failed<br><h1>&#x1f61e;</h1>";
    } else if (score >= 35 && score < 49) {
      document.getElementById('msg').innerHTML = "Congrats you have passed in Class Three..!<br><h1> &#x1f610;</h1>";
    } else if (score >= 50 && score < 59) {
      document.getElementById('msg').innerHTML = "Congrats you have passed in Second class..!!<br><h1>&#x1f60a;</h1>";
    } else if (score >= 60 && score < 79) {
      document.getElementById('msg').innerHTML = "Congrats you have passed in First class..!!<br><h1>&#x1f603;</h1>";
    } else if (score >= 80 && score < 100) {
      document.getElementById('msg').innerHTML = "Congrats you have passed in Distinction class..!!<br><h1>&#x1f60e;</h1>";
    } else if (score == 100) {
      document.getElementById('msg').innerHTML = "Congrats you have Scored 100 out of 100..!!<br><h1>&#x1f60d;</h1>";
    } else if (score == "" || score >100) {
      document.getElementById('msg').innerHTML = "Invalid input";
    }
  }

function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}