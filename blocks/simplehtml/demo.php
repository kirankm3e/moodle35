
<?php
require_once(__DIR__ .'/../../config.php');
global $CFG, $OUTPUT, $USER, $SITE, $PAGE, $DB;
$PAGE->set_context(get_context_instance(CONTEXT_SYSTEM));
$PAGE->set_url('/simplehtml/demo.php');
$PAGE->requires->js('/simplehtml/alert.js');
echo $OUTPUT->header();
echo $OUTPUT->footer();
?>