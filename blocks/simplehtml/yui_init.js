function myfunction() {
    alert('Button clicked');
}


YUI().use(
    function(Y) {
        alert('Hello from YUI ' + Y.version);
    }
);

YUI().use("node-base", function(Y) {  var btn1_Click = function(e)  {    alert('Button clicked');   };  Y.on("click", btn1_Click, "#btn1"); });
YUI().use("node-base", function(Y) {
    Y.on (
        "available", 
        function() {
            printMessage("Element #container 'available'");
        }, "#container"
    );
    Y.on (
        "contentready", 
        function() {
            printMessage("Element #container 'contentready'");
        }, "#container"
    );
    Y.on (
        "domready", 
        function() {
            printMessage("Page 'domready'");
        }
    );
    Y.on (
        "load", 
        function(e) {
            printMessage("Window 'load'");
        }, Y.config.win
    );
    var order = 1;
    var container = Y.one('container');
    function printMessage(message) {
        container.append('<p>' + order++ + '. ' + message + '<p>');
    }
});