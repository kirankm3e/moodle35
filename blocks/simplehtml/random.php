<?php
<?>
<div class="card">
  <div class="inner">
    <div class="profile">
      <div class="avatar">
        <img id="avatar" src="" alt="">
      </div>
      <div class="profile-info">
        <div class="name"></div>
        <div class="location"></div>
        <a class="blog" target="_blank"></a>
      </div>
    </div>
    <div class="count">
      <div class="followers"></div>
      <div class="following"></div>
      <div class="public_repos"></div>
    </div>
  </div>
</div>
<style>
@import url('https://fonts.googleapis.com/css?family=Poppins');

body {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  font-family: 'Poppins', sans-serif;
  color: #494949;
  background: #F5F5F5;
}

.svg-wrapper {
  width: 100px;
  height: 100px;

}
svg {
  transform: rotate(-90deg);
}

.outer-circle {
  cx: 20;
  cy: 15;
  r: 10; 
  fill: none; 
  stroke-dashoffset: 633; 
  stroke: #EEE;
  stroke-width: 3px;
}

.inner-circle {
  // stroke: #FE9096;
  stroke-dasharray: 63;
  cx: 20;
  cy: 15;
  r: 10; 
  fill: none; 
  stroke-width: 3px;
  transition: 0.4s all ease-in-out;
}

.card {
  width: 400px;
  background: white;
  box-shadow: 3px 3px 15px #DDD, -3px -3px 15px #EEE;
}

.avatar {
  width: 100px;

  img {
    width: 100%;
    border-radius: 100%;
  }
}

.inner {
  display: flex;
  flex-direction: column;
  padding: 20px;

  > * {
    margin-bottom: 20px;
  }

  .profile {
    display: flex;
    justify-content: space-around;
    align-items: center;
    font-size: 12px;
  }

  .blog {
    color: #FE9096;
  }

  .count {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  }

  .followers, .following, .public_repos {
    width: 100px;
    height: 150px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    font-size: 12px;

    svg {
      width: 100px;
      height: 100px;
      transition: 1s all ease-in-out;
    }

    p {
      margin: 0;
    }
  }
}

@media (max-width: 500px) {
  .card {
    width: 90%;
  }
  .inner {
    .profile {
      flex-direction: column;
    }
    .count {
      justify-content: center;
    }
  }

  .profile-info {
    text-align: center;
  }
}
</style>

<script>
let card = document.querySelector(".card");
let count = document.querySelector(".count");
let avatarPic = document.getElementById("avatar");
let cir = 2 * Math.PI * 10;

// const proxyurl = "https://cors-anywhere.herokuapp.com/";
// const url = "https://api.github.com/users/sashatran";
// fetch(url)
// .then((res) => res.json())
//   .then((data) => {
//   processData(data);
// }).catch(err => console.log(err));


let data = {
  "login": "sashatran",
  "id": 22401912,
  "avatar_url": "https://avatars0.githubusercontent.com/u/22401912?v=4",
  "url": "https://api.github.com/users/sashatran",
  "name": "Sasha Tran",
  "blog": "http://sashatran.com",
  "location": "San Francisco,CA",
  "bio": "http://codepen.io/sashatran\r\n",
  "public_repos": 40,
  "public_gists": 2,
  "followers": 337,
  "following": 40,
}
function populateCard(data) {
  let avatarUrl = data.avatar_url;
  avatarPic.setAttribute("src", avatarUrl);

  let followers = data.followers;
  let following = data.following;
  let location = data.location;
  for(let item in data) {
    if(item === "followers" || item === "following" || item === "public_repos") {
      createCircle(item, data[item], card.querySelector(`.${item}`));
    }

    else if(card.querySelector(`.${item}`) !== null) {
      let current = card.querySelector(`.${item}`);
      if(item === "blog") {
        current.setAttribute("href", data[item]);
      }
      current.innerHTML = data[item];
    } 
  }
}

function createCircle(content, val, parent) {
  let div = document.createElement("div");
  let p = document.createElement("p");
  let title = document.createElement("div");
  let frag = document.createDocumentFragment();
  let markup = `<svg viewBox="0 0 40 30" xmlns="http://www.w3.org/2000/svg">
<linearGradient id="gradient" x1="0" y1="0" x2="1" y2="1">
<stop offset="0%" stop-color="#F2C061"></stop>
<stop offset="100%" stop-color="#FE9096"></stop>
</linearGradient>
<circle class="outer-circle"></circle>
<circle class="inner-circle circle-${content}" id="circle" stroke="url(#gradient)" stroke-dashoffset=${cir}></circle>
</svg>`;
  div.innerHTML = markup;
  div.className = "svg-wrapper";
  p.innerHTML = val;
  title.innerHTML = content;
  parent.innerHTML = markup;
  frag.appendChild(p);
  frag.appendChild(title);
  parent.appendChild(frag);
}

let circles = ["followers" ,"following", "public_repos"];

function animateStrokes() {
  circles.forEach((circle) => {
    console.log(data[circle]);
    let val = data[circle];
    if(val > 100) {
      val = val / 20;
    }
    let current = document.querySelector(`.circle-${circle}`);
    current.setAttribute("stroke-dashoffset", val);
  })
}

function init() {
  populateCard(data);
  setTimeout(() => {
    animateStrokes()
  }, 1000);
}

init();
</script>