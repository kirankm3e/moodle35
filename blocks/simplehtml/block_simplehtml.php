<?php
require_once(__DIR__.'/../../config.php');
class block_simplehtml extends block_base {
        public function init() {
            $this->title = get_string('simplehtml', 'block_simplehtml');
        }
        public function get_content() {
            GLOBAL $CFG;
            if ($this->content !== null) {
                return $this->content;
            }
            $this->content = new stdClass;
            $this->content->text = 'The content of our simplehtml block<br>';
            $this->content->text .= '<a href ="'.$CFG->wwwroot.'/blocks/simplehtml/redirect.php"><li>Redirect page💙</li></a>';
            $this->content->text .= "<a href =".$CFG->wwwroot."/blocks/simplehtml/webhtml.php><li>Website</li></a>";
            $this->content->text .= "<a href =".$CFG->wwwroot."/blocks/simplehtml/popup.php><li>JavaScript</li></a>";
            $this->content->footer = '3E Software Solutions';
            $this->content->footer .= html_writer::tag('p',
                                        html_writer::link(
                                            new moodle_url(
                                                '/blocks/simplehtml/alert.php?myfunction();'),
                                            'Click Me', 
                                            array('class' => 'btn btn-default')
                                        )
                                  );
            $this->content->footer .= "<script type = 'text/javascript'>myfunction();</script>";

            return $this->content;
        }
        }

