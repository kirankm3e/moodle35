<?php
require_once("$CFG->libdir/formslib.php");
class simplehtml_form extends moodleform {
    //Add elements to form
    public function definition() {
        global $CFG;
 
        $mform = $this->_form; // Don't forget the underscore! 
 
        $mform->addElement('text', 'email', get_string('email')); // Add elements to your form
        $mform->addElement('text', 'username', get_string('username'));
        ?>
        <script> function myfun() {
            alert("hi");
        } </script>
        <?php
        $mform->addElement('password', 'password', get_string('password'));
        $mform->addElement('button', 'Save', get_string('Click me'));
        $mform->addElement('checkbox', 'ratingtime', get_string('ratingtime'));
        $radioarray=array();
$radioarray[] = $mform->createElement('radio', 'yesno', '', get_string('yes'), 1, $attributes);
$radioarray[] = $mform->createElement('radio', 'yesno', '', get_string('no'), 0, $attributes);
$mform->addGroup($radioarray, 'radioar', '', array(' '), false);
$mform->addElement('date_selector', 'assesstimefinish', get_string('to'));
$mform->addElement('duration', 'timelimit', get_string('timelimit', 'quiz'));
$mform->addElement('editor', 'fieldname', get_string('labeltext'));
$mform->setType('fieldname', PARAM_RAW);
$mform->addElement('filepicker', 'userfile', get_string('file'), null, array('maxbytes' => $maxbytes, 'accepted_types' => '*'));
$mform->addElement('modgrade', 'scale', get_string('grade'), false);
$mform->addElement('passwordunmask', 'password', get_string('label'), $attributes);
        $mform->addElement('advcheckbox', 'ratingtime', get_string('ratingtime', 'forum'), 'Label displayed after checkbox', array('group' => 1), array(0, 1));
        $mform->setType('email', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('email', 'Please enter email');        //Default value
    }
    //Custom validation should be added here
    function validation($data, $files) {
        return array();
    }
}