<?php
class simplehtml_form extends moodleform {
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $mform->addElement('text', 'email', get_string('email'));
        $mform->setType('email', PARAM_NOTAGS);
        $mform->setDefault('email', 'Please enter mail');
        
    }

    function validation($data, $files) {
        return array();
    }
}