<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');
require_once($CFG->dirroot . '/mod/quiz/mod_form.php');
$myform = new mod_quiz_mod_form();
$myform->display();
echo $OUTPUT->header();
echo $OUTPUT->footer();